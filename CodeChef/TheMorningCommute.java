import java.util.*;
class TheMorningCommute{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0){
			
			int n=scan.nextInt();
			int sum=0;
			
			for(int i=0;i<n;i++){
				int a=scan.nextInt();
				int b=scan.nextInt();
				int c=scan.nextInt();
				
				if(i==0)
				{
					sum=a+b;
					continue;
				}
				if(sum<=a)
					sum=a+b;
				else{
					while(a<sum)
						a=a+c;
					sum=b+a;
				}
			
				
			}
			
				System.out.println(sum);
		}//while(testCase-->0)
	}//main()
}//class
