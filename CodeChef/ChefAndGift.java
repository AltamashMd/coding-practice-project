import java.util.*;
class ChefAndGift
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0)
		{
			int n=scan.nextInt();
			int k=scan.nextInt();
			int arr[]=new int[n];
			int even=0;
			
			for(int i=0;i<n;i++){
				arr[i]=scan.nextInt();
					if(arr[i]%2==0){
						even++;
					}
			}
			
			if(k==0 && even!=n){
				System.out.println("YES");
			}
			else if(k<=even && k!=0){
				System.out.println("YES");
			}
			else{
				System.out.println("NO");
			}
				
		}//while(testCase-->0)
		
	}
	
	
	
	

}//class