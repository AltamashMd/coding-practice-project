import java.util.*;
class gcd
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
		int a=scan.nextInt();
		int b=scan.nextInt();
		int result=gcd(a,b);
		
		System.out.println(result);
	}
	
	public static int gcd(int a,int b){
		
		return (b==0 ? a : gcd(b,a%b));
	}

}
