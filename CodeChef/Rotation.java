import java.util.*;
 public class Rotation
 {
	 public static void main(String arg[])
	 {
		 Scanner scan=new Scanner(System.in);
		 
		 int size=scan.nextInt();
		 int d=scan.nextInt();
		 int arr[]=new int[size];
		 
		 for(int i=0;i<size;i++)
		 {
			 arr[i]=scan.nextInt();
		 }
		 //rotate(arr,size,d);
		 
		 for(int i=0;i<d;i++)
		 {
			 leftRotate(arr,size);
		 }
		 for(int i=0;i<size;i++){
			 
			 System.out.print(arr[i]+" ");
		 }
	 }
	 
	 public static void rotate(int arr[],int size,int d)
	 {
		int tempArray[]=new int[d];
		int temp=d;
		int i;
		for(i=0;i<d;i++)
		{
			tempArray[i]=arr[i];
		}
		for(i=0;i<(size-d);i++)
		{
			arr[i]=arr[temp];
			temp++;
		}
		for(int j=0;j<d;j++)
		{
			arr[i]=tempArray[j];
			i++;
		}
		
	 }
	 
	 public static void leftRotate(int arr[],int size)
	 {
		 int temp=arr[0];
		 int i;
		 for(i=0;i<size-1;i++)
		 {
			 arr[i]=arr[i+1];
		 }
		 arr[i]=temp;
	 }
 }