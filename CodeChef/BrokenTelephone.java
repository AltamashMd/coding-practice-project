import java.util.*;
class BrokenTelephone{
	public static void main(String arg[]){
		Scanner input=new Scanner(System.in);
		int testCase=input.nextInt();
		
		while(testCase-->0){
			
			int N=input.nextInt();
			int arr[]=new int[N];
			int count=0;
			
			for(int i=0;i<N;i++){
				arr[i]=input.nextInt();
			}
			
			int x=-1;
			for(int i=0;i<N-1;i++){
				
				if(arr[i]!=arr[i+1] && x!=i){
					count=count+2;
					x=i+1;
				}
				else if(arr[i]!=arr[i+1] && x==i){
					count=count+1;
					x=i+1;
				}
				
			}
			
			System.out.println(count);
			
				
		}//while(testCase-->0)
	}//main()
}//class
