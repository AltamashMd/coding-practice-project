import java.util.*;
public class TheTwoWaterJugPuzzle
{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testcase=scan.nextInt();
		
		while(testcase-- >0){
		int m=scan.nextInt();
		int n=scan.nextInt();
		int d=scan.nextInt();
		
		int result=minStep(m,n,d);
		
		System.out.println(result);
		}
	}
	
	public static int minStep(int m,int n,int d)
	{
		if(m>n)
		{
			int temp=m;
			m=n;
			n=temp;
		}
		if(d>n)
		{
			return -1;
		}
		if(d%gcd(n,m)!=0)
		{
			return -1;
		}
		return Math.min(pour(m,n,d),pour(n,m,d));
	}
	
	public static int gcd(int a,int b)
	{
		return (b==0 ? a : gcd(b,a%b));
	}
	public static int pour(int fromCap,int toCap,int d)
	{
		int from=fromCap;
		int to=0;
		int step=1;
		
		while(from!=d && to!=d)
		{
			int temp=Math.min(from,toCap-to);
			
			to=to+temp;
			from=from-temp;
			step++;
			
			if(from==d || to==d)
			{
				break;
			}
			
			if(from==0)
			{
				from=fromCap;
				step++;
			}
			if(to==toCap)
			{
				to=0;
				step++;
			}
		}
		return step;
	}
}