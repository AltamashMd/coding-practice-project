import java.util.*;
class RecipeReconstruction
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int i;
		int j;
		
		while(testCase-->0)
		{
				int result=1;
				String str=scan.next();
				i=0;
				j=(str.length()-1);
				while(i<=j){
					
					if((str.charAt(i)==str.charAt(j))){
						
						if((str.charAt(i)=='?') && (str.charAt(j)=='?')){
							
							result=result*26;
							result=result%10000009;
						}
						else{
							
							result=result*1;
							result=result%10000009;
						}
					}
					else{
						
						if((str.charAt(i)=='?') && (str.charAt(j)!='?')){
							
							result=result*1;
							result=result%10000009;
						}
						else if((str.charAt(i)!='?') && (str.charAt(j)=='?')){
							
							result=result*1;
							result=result%10000009;
						}
						else{
							result=0;
							break;
						}
					}
			
					i++;
					j--;
				}
				
				System.out.println(result);
				
		}//while(testCase-->0)
		
	}
	
	
	
	

}//class