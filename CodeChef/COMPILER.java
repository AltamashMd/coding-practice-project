import java.util.Scanner;
import java.util.Stack;
 
 class COMPILER {
 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in= new Scanner(System.in);
		
		int t=in.nextInt();
		in.nextLine();
		while(t!=0) {
			String s=in.nextLine();
			
			Stack<Character> stack= new Stack<Character>();
			
			int c=0;
			int k=0;
			for(int i=0;i<s.length();i++) {
				
				if(s.charAt(i)=='<') {
					stack.push(s.charAt(i));
				}
				
				else {
					if(stack.isEmpty()) {
						break;
					}
					
					stack.pop();
					c+=2;
					if(stack.isEmpty()) {
						k=c;
					}
				}
				
				
			
			}
			
			System.out.println(k);
			
			t--;
		}
 
	}
 
}