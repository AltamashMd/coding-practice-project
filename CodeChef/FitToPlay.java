import java.util.*;
class FitToPlay
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0)
		{
			int N=scan.nextInt();
			int arr[]=new int[N];
			int minm=1000001;
			int ans=0;
			
			for(int i=0;i<N;i++){
				arr[i]=scan.nextInt();
				ans=Math.max(ans,arr[i]-minm);
				minm=Math.min(minm,arr[i]);
			}
			
			
			if(ans<=0){
				System.out.println("UNFIT");
			}
			else{
				System.out.println(ans);
			}
				
		}//while(testCase-->0)
		
	}
	
	
	
	

}//class