import java.util.*;
class ChefAndGround
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				int N=scan.nextInt();
				int M=scan.nextInt();
				int arr[]=new int[N];
				
				
				for(int i=0;i<N;i++)
				{
					arr[i]=scan.nextInt();
					
				}
				
				Arrays.sort(arr);
				int max=arr[N-1];
				
				for(int i=0;i<N-1;i++)
				{
					if(arr[i]<max)
					{
						M=M-(max-arr[i]);
					}
				}
				
				if(M==0 || (M>0 && M%N==0))
				{
					System.out.println("Yes");
				}
				else
				{
					System.out.println("No");
				}
				
			}			
	}
	
	
}
