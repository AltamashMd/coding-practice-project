import java.util.Scanner;
class yourNameIsMine{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int i,j,count;
		while(testCase-->0){
			count=0;
			String s1=scan.next();
			String s2=scan.next();
			if(s1.length()==s2.length()){
				if(s1.equals(s2)){
					System.out.println("YES");
				}//if(inner)
				else{
					System.out.println("NO");
				}//else
			}//if(outer)
			else{
				int start=0;
				if(s1.length()<s2.length()){
					for(i=0;i<s1.length();i++){
						for(j=start;j<s2.length();j++){
							if(s1.charAt(i)==s2.charAt(j)){
								count++;
								start=j+1;
								break;
							}
						}
					}
					if(s1.length()==count){
						System.out.println("YES");
					}
					else{
						System.out.println("NO");
					}//else
				}//if(s1.length()<s2.length())
				else{
					start=0;
					for(i=0;i<s2.length();i++){
						for(j=start;j<s1.length();j++){
							if(s2.charAt(i)==s1.charAt(j)){
								count++;
								start=j+1;
								break;
							}
						}
					}
					if(s2.length()==count){
						System.out.println("YES");
					}
					else{
						System.out.println("NO");
					}//else
				}//else	
			}//else
		}//while(testCase!=0)
	}//main()
}//class
