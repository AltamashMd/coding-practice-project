import java.util.*;
class ArrangingCupCakes
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		
		while(testCase-->0)
		{
			
			int N=scan.nextInt();
			int min=N-1;
			for(int x=(int)Math.sqrt(N);x>0;x--)
			{
				if(N%x==0)
				{
					int y=N/x;
						min=y-x;
						break;
				}
			}
			System.out.println(min);
			
		}//while(testCase-->0)
		
	}
}//class