import java.util.*;
class ArraySubsetOfAnotherArray
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				int sizeOfFirstArray=input.nextInt();
				int sizeOfSecondArray=input.nextInt();
				
				int arr1[]=new int[sizeOfFirstArray];
				int arr2[]=new int[sizeOfSecondArray];
				
				for(int i=0;i<sizeOfFirstArray;i++)
				{
					arr1[i]=input.nextInt();
				}
				
				for(int i=0;i<sizeOfSecondArray;i++)
				{
					arr2[i]=input.nextInt();
				}
				
				Arrays.sort(arr1);
				int flag=0;
				for(int i=0;i<sizeOfSecondArray;i++)
				{
					boolean status=Search(arr2[i],arr1,sizeOfFirstArray);
					
					if(status==false)
					{
						System.out.println("No");
						flag=1;
						break;
					}
				}
				
				if(flag==0)
				{
					System.out.println("Yes");
					
				}
				
				
			}
	}
	
	public static boolean Search(int search, int arr[], int size)
	{
		int low=0;
		int high=size-1;
		
		while(low<high)
		{
			int mid=(low+high)/2;
			
			if(arr[mid]==search)
			{
				return true;
			}
			else if(search>arr[mid])
			{
				low=mid+1;
			}
			else
			{
				high=mid-1;
			}
		}
		
		return false;
	}
}