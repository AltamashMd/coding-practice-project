import java.util.*;
class SumQueries
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
		long result;
		long N=scan.nextLong();
		int M=scan.nextInt();
	
		while(M-- >0){
			
			long q=scan.nextLong();
			if(q>=(N+2) && q<=(3*N)){
				
				if(q<=(2*N+1)){
					
					result=(q-(N+1));
				}
				else{
					
					result=((3*N+1)-q);
				}
				
				System.out.println(result);
			}
			else{
				
				System.out.println(0);
			}
		}
	}
	
}//class