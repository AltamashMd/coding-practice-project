import java.util.*;
class ThreeDifferentNumbers{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0){
			
			long num[]=new long[3];
			
			for(int i=0;i<3;i++){
				
				num[i]=scan.nextLong();
			}
			
			Arrays.sort(num);
			
			long A=num[0]%1000000007;
			long B=(num[1]-1)%1000000007;
			long C=(num[2]-2)%1000000007;
			
			long result=(A*B)%1000000007;
			
			long finalResult=(result*C)%1000000007;
			
			System.out.println(finalResult);
				
		}//while(testCase-->0)
	}//main()
}//class
