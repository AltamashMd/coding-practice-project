import java.util.*;

class MaxAndNextMax{
	
	public static void main(String arg[]){
		
		Scanner scan=new Scanner(System.in);
		
		int n=scan.nextInt();
		int arr[]=new int[2*n];
		
		for(int i=n;i<=(2*n-1);i++){
			
			arr[i]=scan.nextInt();
		}
		
		buildTreeStructure(arr,n);
		
		System.out.println("MAX ="+arr[1]);
		System.out.println("NEXTMAX ="+nextMax(arr,n));
	}
	
	public static void buildTreeStructure(int arr[],int n){
		
		for(int i=(2*n-2);i>1;i=i-2){
			arr[i/2]=Math.max(arr[i],arr[i+1]);
		}
	}
	public static int nextMax(int arr[],int n){
		
		int i=2;
		int next=Math.min(arr[2],arr[3]);
		while(i>=(2*n-1)){
			
			if(arr[i]>arr[i+1]){
				next=Math.max(arr[i+1],next);
				i=2*i;
			}
			else if(arr[i+1]>arr[i]){
				next=Math.max(arr[i],next);
				i=2*(i+1);
			}
		}
		return next;
	}
}