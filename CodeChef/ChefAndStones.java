import java.util.*;
class ChefAndStones
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				
				int N=scan.nextInt();
				
				int K=scan.nextInt();
				
				int typesOfStone[]=new int[N];
				
				int costOfStones[]=new int[N];
				
				for(int i=0;i<N;i++)
				{
					typesOfStone[i]=scan.nextInt();
					
				}
				
				for(int i=0;i<N;i++)
				{
					costOfStones[i]=scan.nextInt();
					
				}
				
				long max=0;
				long X=0;
				long Y=0;
				for(int i=0;i<N;i++)
				{
					X=K/typesOfStone[i];
					Y=X*costOfStones[i];
					
					if(Y>max){
						max=Y;
					}
				}
				
				System.out.println(max);
				
			}
	}
}
