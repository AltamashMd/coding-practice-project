import java.util.*;
class TheWayToFriendHouseNeverLong
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				
				System.out.println();
				int N=scan.nextInt();
				int arr[][]=new int[N][2];
				int temp[]=new int[2];
				
				for(int i=0;i<N;i++)
				{
					for(int j=0;j<2;j++)
					{
						arr[i][j]=scan.nextInt();
					}
				}
				
				for(int i=0;i<N;i++)
				{
					for(int j=i+1;j<N;j++)
					{
						if(arr[i][0]>=arr[j][0])
						{
							if((arr[i][0]==arr[j][0]) && arr[i][1]>arr[j][1])
							{
								continue;
							}
							else
							{
								temp=arr[i];
								arr[i]=arr[j];
								arr[j]=temp;
							}
						}
					}
				}
				
				double sum=0;
				for(int i=0;i<N-1;i++)
				{
					
					double distanceOfTwoPoints = (double)calculateDistance(arr[i][0],arr[i][1],arr[i+1][0],arr[i+1][1]);
					
					sum= sum + distanceOfTwoPoints;
				}
				
				
					double result=(double)Math.round(sum*100.0)/100.0;
					System.out.println(result);
				
				
			}
	}
	
	public static double calculateDistance(int x1,int y1,int x2,int y2)
	{
		double d1=(double)Math.pow((x2-x1),2);
		
		double d2=(double)Math.pow((y2-y1),2);
		
		double distance=(double)Math.sqrt(d1+d2);
		
		return distance;
	}
}
