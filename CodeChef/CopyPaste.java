import java.util.Scanner;
class CopyPaste{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int i;
		while(testCase-->0){
			int count=0;
			int n=scan.nextInt();
			int arr[]=new int[100001];
			for(i=0;i<n;i++){
				int temp=scan.nextInt();
				arr[temp]++;
			}
			for(i=1;i<=100000;i++){
				if(arr[i]>0)
					count++;
				if(count>=n)
					break;
			}
			System.out.println(count);
		}//while()
	}//main()
}//class
