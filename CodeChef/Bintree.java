import java.util.*;
class Bintree
{ 	
	public static void main(String[] args) 
	{ 	
		Scanner scan=new Scanner(System.in);
		int test=scan.nextInt();
		while(test-- >0)
		{
			int k=0;
			int i=scan.nextInt();
			int j=scan.nextInt();
			
			String binaryOfi=Integer.toBinaryString(i);
			
			String binaryOfj=Integer.toBinaryString(j);
			
			int n=binaryOfi.length();
			
			int m=binaryOfj.length();
			
			for(int p=0;p<(Math.min(n,m));p++){
				
				if((binaryOfi.charAt(p))==(binaryOfj.charAt(p)))
				{
					k++;
				}
				else
				{
					break;
				}
			}
			
			int shortestPath=((n-k)+(m-k));
			System.out.println(shortestPath);
		}
	}
}
