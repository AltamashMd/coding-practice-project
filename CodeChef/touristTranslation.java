import java.util.*;
class touristTranslation{
	public static void main(String[] args){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		String str=scan.next();
		while(testCase-->0){
			StringBuffer sb=new StringBuffer();
			String substr=scan.next();
			for(int i=0;i<substr.length();i++)
			{
				char ch=substr.charAt(i);
				if(Character.isAlphabetic(ch)){
					if(Character.isLowerCase(ch)){
						int n=substr.charAt(i);
						int n1=n-97;
						char ch1=str.charAt(n1);
						sb.append(ch1);
					}
					else{
						int n=substr.charAt(i);
						int n1=n-65;
						char ch1=Character.toUpperCase(str.charAt(n1));
						sb.append(ch1);
					}
				}
				else{
					if(ch=='_'){
						sb.append(' ');
					}
					else{
						sb.append(ch);
					}
				}
			}
			System.out.println(sb);
			
		}//while()
	}//main()
}//class