import java.util.*;
class ChefAndSubarray
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int N=scan.nextInt();
			int arr[]=new int[N+1];
			int zero=0;
			for(int i=0;i<N;i++){
				
				arr[i]=scan.nextInt();
				if(arr[i]==0){
					zero++;
				}
			}
			
			int count=0;
			int max=0;
			
			
			
			for(int i=0;i<(N-1);){
				
				count=0;
				
				if(((arr[i])*(arr[i+1]))>0){
					
					while(((arr[i])*(arr[i+1]))!=0){
						
						count++;
						i++;
					}
					i++;
				}
				else{
					i++;
				}
				
				if(count>max){
					max=count;
				}
			}
			
			if(max==0 && zero!=N){
				System.out.println(1);
			}
			else if(zero==N){
				System.out.println(0);
			}
			else{
				System.out.println(max+1);
			}
			
		
	}
	
	
	

}//class