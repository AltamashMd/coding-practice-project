import java.util.*;
import java.math.BigInteger;
class GCD2{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		while(testCase-->0){
			BigInteger A=scan.nextBigInteger();
			BigInteger B=scan.nextBigInteger();
			System.out.println(gcd(A,B));
		}//while()
	}//main()
	static int gcd(BigInteger a, BigInteger b)
	{
	if (b.equal(0))
		return a;
	else
		return gcd(b,a.mod(b));
	}
}//class
