import java.util.*;
class SurajGoesShopping
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int i;
		int j;
		
		
		while(testCase-->0)
		{
			int sum=0;
			int noOfItems=scan.nextInt();
			int CostOfItems[]=new int[noOfItems];
			
			for(i=0;i<noOfItems;i++){
				
				CostOfItems[i]=scan.nextInt();
			}
			for(i=0;i<noOfItems;i++){
				for(j=i+1;j<noOfItems;j++){
					
					if(CostOfItems[i]<CostOfItems[j]){
						
						int temp=CostOfItems[i];
						CostOfItems[i]=CostOfItems[j];
						CostOfItems[j]=temp;
					}
				}
			}
			
			
			if(noOfItems>=4){
				i=0;
				while(noOfItems>=4){
					
					sum=sum+CostOfItems[i]+CostOfItems[i+1];
					noOfItems=noOfItems-4;
					i=i+4;
				}
				if(noOfItems>1){
					sum=sum+CostOfItems[i]+CostOfItems[i+1];
				}
				else if(noOfItems>0){
					sum=sum+CostOfItems[i];
				}
			}
			else{
				
				if(noOfItems>1){
					sum=sum+CostOfItems[0]+CostOfItems[1];
				}
				else if (noOfItems>0){
					sum=sum+CostOfItems[0];
				}
			}
			
			System.out.println(sum);
		}//while(testCase-->0)
		
	}
	
	
	

}//class