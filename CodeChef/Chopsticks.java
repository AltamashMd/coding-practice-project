import java.util.*;
class Chopsticks
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
			
			int N=scan.nextInt();
			int D=scan.nextInt();
			int count=0;
			int arr[]=new int[N+1];
			for(int i=0;i<N;i++)
			{
				arr[i]=scan.nextInt();
			}
			
			merge_sort(arr,0,N-1);
			
			for(int i=0;i<N-1;i++)
			{
				if((arr[i+1]-arr[i])<=D)
				{
					count++;
					i++;
				}
			}
			
			System.out.println(count);		
	}
	
	public static void merge_sort(int arr[],int p,int r)
	{
		int q;
		if(p<r)
		{
			q=(p+r)/2;
			merge_sort(arr,p,q);
			merge_sort(arr,q+1,r);
			merge(arr,p,q,r);
		}
	}
	
	public static void merge(int arr[],int p,int q,int r)
	{
		int k,i,j;
		int n1=q-p+1;
		int n2=r-q;
		int L[]=new int[n1+2];
		int R[]=new int[n2+2];
		for(i=1;i<=n1;i++){
			L[i]=arr[p+i-1];
		}
		for(j=1;j<=n2;j++){
			R[j]=arr[q+j];
		}
		L[n1+1]=1000000001;
		R[n2+1]=1000000001;
		i=1;
		j=1;
		for(k=p;k<=r;k++)
		{
			if(L[i]<=R[j])
			{
				arr[k]=L[i];
				i++;
			}
			else
			{
				arr[k]=R[j];
				j++;
			}
		}
	}
}//class