import java.util.*;
class NotATriangle
{
	static int result;
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int N;
			while((N=scan.nextInt())!=0)
			{
				result=0;
				int arr[]=new int[N];
				boolean used[]=new boolean[N];
				int count=0;
				int largest=0;
				
				for(int i=0;i<N;i++)
				{
					arr[i]=scan.nextInt();
					used[i]=false;
				}
				
				if(N>3)
				{
					printAllSubset(arr,used,0,0,3);
					System.out.println(result);
				}
				else
				{
						int a=arr[0];
						int b=arr[1];
						int c=arr[2];
					
						if(a>b && a>c)
						{
							largest=a;
							if((b+c)<largest)
							{
								count++;
							}
						}
						else if(b>a && b>c)
						{
							largest=b;
							if((a+c)<largest)
							{
								count++;
							}
						}
						else
						{
							largest=c;
							if((a+b)<largest)
							{
								count++;
							}
						}
				
					System.out.println(count);
				}
			
				
				
			}
	}
	
	public static void printAllSubset(int arr[],boolean[] used,int start,int currentIndex,int k)
	{
		int a=0;
		int b=0;
		int c=0;
		int flag=0;
		int count=0;
		int largest=0;
		if(currentIndex==k)
		{
			for(int i=0;i<arr.length;i++)
			{
				if(used[i])
				{
					if(flag==0)
					{
						a=arr[i];
						flag=1;
					}
					else if(flag==1)
					{
						b=arr[i];
						flag=2;
					}
					else if(flag==2)
					{
						c=arr[i];
					}
					
				}
				
			}
						if(a>b && a>c)
						{
							largest=a;
							if(b+c<largest)
							{
								count=count+1;
							}
						}
						else if(b>a && b>c)
						{
							largest=b;
							if(a+c<largest)
							{
								count=count+1;
							}
						}
						else
						{
							largest=c;
							if(a+b<largest)
							{
								count=count+1;
							}
						}
						
					result=result+count;					
			return;
		}
		
		if(start==arr.length)
		{
			return;
		}
		
		used[start]=true;
		printAllSubset(arr,used,start+1,currentIndex+1,k);
		used[start]=false;
		printAllSubset(arr,used,start+1,currentIndex,k);
	}
	
	
	
	

}//class