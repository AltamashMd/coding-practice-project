import java.util.*;
class ChefAndKingship
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				
				int N=scan.nextInt();
				long arr[]=new long[N];
				
				for(int i=0;i<N;i++)
				{
					arr[i]=scan.nextLong();
				}
				
				Arrays.sort(arr);
				
				long sum=0;
				for(int i=1;i<N;i++)
				{
					sum = (long)(sum + arr[0]*arr[i]);
				}
				
				System.out.println(sum);
				
			}
	}
}
