import java.util.*;
class ChefAndNotebooks
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int flag;
		
		
		
		while(testCase-->0)
		{
			flag=0;
			int totalPageRequired=scan.nextInt();
			int pageRemaining=scan.nextInt();
			int budget=scan.nextInt();
			int numberOfNotebooks=scan.nextInt();
			
			for(int i=0;i<numberOfNotebooks;i++){
				
				int pagesInEachNotebook=scan.nextInt();
				int costOfEachNotebook=scan.nextInt();
				
				int pageRequired=(totalPageRequired-pageRemaining);
				if((pagesInEachNotebook>=pageRequired) && (costOfEachNotebook<=budget)){
						flag=1;
				}
			}
			if(flag==1){
				
				System.out.println("LuckyChef");
			}
			else{
				System.out.println("UnluckyChef");
			}

				
		}//while(testCase-->0)
		
	}
	
	
	
	

}//class