import java.util.*;

class HeapSort{
	
	public static void main(String arg[]){
		
		Scanner scan=new Scanner(System.in);
		
		int low=0;
		int n=scan.nextInt();
		int arr[]=new int[2*n+1];
		
		for(int i=n;i<=(2*n-1);i++){
			
			arr[i]=scan.nextInt();
		}
		
		buildTreeStructure(arr,n);
		
		
		
		for(int i=1;i<=n;i++){
			System.out.println(arr[1]);
			GetnextMax(arr,n,low);
		}
	}
	
	public static void buildTreeStructure(int arr[],int n){
		
		int i;
		for(i=(2*n-2);i>1;i=i-2){
			arr[i/2]=Math.max(arr[i],arr[i+1]);
		}
	}
	public static void GetnextMax(int arr[],int n,int low){
		
		int i=2;
		while(i<=(2*n-1)){
			
			if(arr[i]>arr[i+1]){
				arr[i]=low;
				i=2*i;
			}
			else if(arr[i+1]>arr[i]){
				arr[i+1]=low;
				i=2*(i+1);
			}
		}
		
		for(i=i/2;i>1;i=i/2){
			if(i%2==0)
				arr[i/2]=Math.max(arr[i],arr[i+1]);
			else
				arr[i/2]=Math.max(arr[i],arr[i-1]);
		}
	}
}