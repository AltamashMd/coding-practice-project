import java.util.*;
class TreeSetDemo{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		while(testCase-->0){
			int n=scan.nextInt();
			Set<Integer> s=new TreeSet<>();
			for(int i=0;i<n;i++){
			int temp=scan.nextInt();
			s.add(temp);
			}
			System.out.println(s.size());
		}//while()
	}//main()
}//class
