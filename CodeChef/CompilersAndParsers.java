import java.util.*;
class CompilersAndParsers
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				int count=0;
				int k=0;
				String expression=scan.next();
				Stack<Character> stack=new Stack<Character>();
				
				for(int i=0;i<expression.length();i++)
				{
					if(expression.charAt(i)=='<')
					{
						stack.push(expression.charAt(i));
					}
					else
					{
						if(stack.isEmpty())
						{
							break;
						}
						
						stack.pop();
						count +=2;
						if(stack.isEmpty())
						{
							k=count;
						}
					}
					
				}
					System.out.println(k);
			}			
	}
	
	
}
