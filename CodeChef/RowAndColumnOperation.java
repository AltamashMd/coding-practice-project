import java.util.*;

class RowAndColumnOperation
{ 	
	public static void main(String[] args) 
	{ 	
		Scanner scan=new Scanner(System.in);
		
		int N=scan.nextInt();
		int Q=scan.nextInt();
		
		int row[]=new int[N];
		int column[]=new int[N];
		int maxRow=0;
		int maxCol=0;
		
		while(Q-- >0)
		{
			
			String operation=scan.next();
			

				
			int A=scan.nextInt();
			int X=scan.nextInt();
				
			if(operation.equals("RowAdd"))
			{
					row[A-1]= row[A-1] + X;
					if((row[A-1])>maxRow)
					{
						maxRow=row[A-1];
					}
			}
			else if(operation.equals("ColAdd"))
			{
					column[A-1]= column[A-1] + X;
					if((column[A-1])>maxCol)
					{
						maxCol=column[A-1];
					}
			}
				
		}
		
		System.out.println((maxCol+maxRow));
	}
}