import java.util.*;
class LongestCommonPattern
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
					StringBuilder str1=new StringBuilder(scan.next());
					StringBuilder str2=new StringBuilder(scan.next());
				
					int count=0;
				
					for(int i=0;i<str1.length();i++)
					{
						for(int j=0;j<str2.length();j++)
						{
							if(str1.charAt(i)==str2.charAt(j))
							{
								count++;
								str2.replace(j,j+1,"?");
								System.out.println(str2);
								break;
						
							}
						}
					}
					
				
				System.out.println(count);
			}			
	}
	
	
}
