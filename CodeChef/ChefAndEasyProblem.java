import java.util.*;
class ChefAndEasyProblem
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				long sum=0;
				int N=scan.nextInt();
				long arr[]=new long[N];
				
				for(int i=0;i<N;i++)
				{
					arr[i]=scan.nextInt();
				}
				
				Arrays.sort(arr);
				
				for(int i=(N-1);i>=0;i=i-2)
				{
					sum=(long)(sum + arr[i]);
				}
				
				System.out.println(sum);
			}
	}
}