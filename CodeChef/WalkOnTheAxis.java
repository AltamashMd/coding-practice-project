import java.util.*;
class WalkOnTheAxis
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		
		while(testCase-->0)
		{
			
			long N=scan.nextLong();
			long d1=N;
			long d2=(N+1);
			long d3=(d1*d2);
			long d4=(d3/2);
			long distance=(d4+N);
			System.out.println(distance);
			
		}//while(testCase-->0)
		
	}
}//class