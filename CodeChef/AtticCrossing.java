import java.util.*;
class AtticCrossing
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		
		
		while(testCase-->0)
		{
			String P=scan.next();
			int day=0;
			int count=0;
			int previousCount=0;
			for(int i=0;i<P.length();){
				
				
				count=0;
				if(P.charAt(i)=='.'){
					
					count++;
					i++;
					while(P.charAt(i)!='#'){
						count++;
						i++;
					}
					
				}
				else{
					i++;
				}
				if(count>previousCount){
					day++;
					previousCount=count;
				}
				//System.out.println(count);
				//System.out.println(previousCount);
			}
			
			System.out.println(day);
			
		}//while(testCase-->0)
		
	}
	
	
	

}//class