import java.util.*;
class ReversingDirections{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0){
			
			int N=scan.nextInt();
			String fullDirections[]=new String[N+1];
			String initialDirections[]=new String[N+1];
			String eachDestination[]=new String[N+1];
			
			for(int i=0;i<=N;i++){
				
				fullDirections[i]=scan.nextLine();
			}
			
			for(int i=1;i<=N;i++){
				
				String[] eachDirection=fullDirections[i].split(" ",3);
			
				initialDirections[i]=eachDirection[0];
				eachDestination[i]=eachDirection[(eachDirection.length)-1];
			}
			
			
			int j=0;
			for(int i=1;i<=N;i++){
				
				if(i==1){
					System.out.println("Begin on "+eachDestination[N]);
				}
				else{
				
					if(initialDirections[N-j].equals("Left")){
						System.out.println("Right on "+eachDestination[N-1-j]);
					}
					else{
						System.out.println("Left on "+eachDestination[N-1-j]);
					}	
					j++;
				}
			}
			System.out.println();	
		}//while(testCase-->0)
	}//main()
}//class