import java.util.*;
class PayingUp{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int i,j,sum;
		while(testCase-->0){
			int flag=0;
			int n=scan.nextInt();
			int arr[]=new int[n];
			int requiredSum=scan.nextInt();
			for(i=0;i<n;i++){
				arr[i]=scan.nextInt();
			}
			int total=(1<<n);
			Outer: for(i=0;i<total;i++){
					sum=0;
				Inner: for(j=0;j<n;j++){
						int x=(i & (1<<j));
						//System.out.println(x);
						if(x!=0){
							sum=sum+arr[j];
						}
					
				}//for
				if(sum==requiredSum){
					flag=1;
					break Outer;
				}
			}//for
			if(flag==1)
				System.out.println("Yes");
			else
				System.out.println("No");
		}//while(testCase-->0)
	}//main()
}//class
