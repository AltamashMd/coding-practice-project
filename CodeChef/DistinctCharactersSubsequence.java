import java.util.*;
class DistinctCharactersSubsequence{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0){
			
			String str=scan.next();
			int max=0;
			int count=0;
			
			Set<Character> setOfDistinctChar=new HashSet<>();
			
			for(int i=0;i<str.length();i++){
				if(setOfDistinctChar.add(str.charAt(i))){
					count++;
				}
			}
			
			System.out.println(count);
			
				
		}//while(testCase-->0)
	}//main()
}//class
