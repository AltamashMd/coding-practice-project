import java.util.*;
class AndrewAndMeatballs
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int testcase=scan.nextInt();
			while(testcase-- >0)
			{
				int N=scan.nextInt();
				
				long M=scan.nextLong();
				
				long arr[]=new long[N];
				long sum=0;
				int count=0;
				int COUNT=N;
				int flag=0;
				
				for(int i=0;i<N;i++)
				{
					arr[i]=scan.nextLong();
				}
				
				int total=(1<<N);
				
				for(int i=0;i<total;i++)
				{
					sum=0;
					for(int j=0;j<N;j++)
					{
							int x=(i & (1<<j));
							if(x!=0)
							{
								sum=sum+arr[j];
							}
					}
					
					if(sum>=M)
					{
						
						String binary=Integer.toBinaryString(i);
						flag=1;
						count=0;
						for(int k=0;k<binary.length();k++)
						{
							if(binary.charAt(k)=='1')
							{
								count++;
							}
						}
						if(count<COUNT)
						{
							COUNT=count;
						}
					}
				}
				if(flag==1){
				
					System.out.println(COUNT);
				}
				else{
					System.out.println(-1);
				}
			}
	}
}