import java.util.*;
class PleaseLikeMe
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		while(testCase-->0)
		{
			long L=scan.nextLong();
			long D=scan.nextLong();
			long S=scan.nextLong();
			long C=scan.nextLong();
			
			long sum=S;
			for(long i=1;i<D;i++)
			{
				if(sum>=L)
				{
					break;
				}
				sum=(sum+sum*C);
			}
			
		if(sum>=L)
		{
			System.out.println("ALIVE AND KICKING");
		}
		else
		{
			System.out.println("DEAD AND ROTTING");
		}
			
			
		}//while(testCase-->0)
		
	}
}//class