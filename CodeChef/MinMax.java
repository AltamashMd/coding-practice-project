import java.util.*;

class MinMax{
	
	public static void main(String arg[]){
		
		Scanner scan=new Scanner(System.in);
		
		
		int n=scan.nextInt();
		int max=scan.nextInt();
		int min=max;
		int m=n-1;
		if(n%2==0){
			min=scan.nextInt();
			m=m-1;
		}
		
		for(int i=0;i<m/2;i++){
			int num1=scan.nextInt();
			int num2=scan.nextInt();
			
			if(num1>num2){
				
				if(num1>max){
					max=num1;
				}
				if(num2<min){
					min=num2;
				}
			}
			else{
				
				if(num2>max){
					max=num2;
				}
				if(num1<min){
					min=num2;
				}
			}
		}
		System.out.println("MAX ="+max+" "+"MIN ="+min);
	}
}