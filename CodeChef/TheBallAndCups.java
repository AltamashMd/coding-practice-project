import java.util.*;
class TheBallAndCups
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		while(testCase-->0)
		{
			
			int temp;
			int currentPosition;
			int N=scan.nextInt();
			int arr[]=new int[N+1];
			for(int i=1;i<=N;i++){
				arr[i]=i;
			}
			int C=scan.nextInt();
			int Q=scan.nextInt();
			currentPosition=C;
			for(int i=0;i<Q;i++)
			{
				int L=scan.nextInt();
				int R=scan.nextInt();
				while(L<R)
				{
					if(L>currentPosition || R<currentPosition){
						break;
					}
					if(arr[L]==C || arr[R]==C)
					{
						temp=arr[L];
						arr[L]=arr[R];
						arr[R]=temp;
						if(C==arr[R])
						{
							currentPosition=R;
						}
						else
						{
							currentPosition=L;
						}
						break;
					}
					L++;
					R--;
				}	
				
			}
			System.out.println(currentPosition);
			
		}//while(testCase-->0)
		
	}
}//class