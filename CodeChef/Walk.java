import java.util.*;
class Walk
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		
		while(testCase-->0)
		{
			int N=scan.nextInt();
			int W[]=new int[N];
			int max=scan.nextInt();
			for(int i=1;i<N;i++){
				
				W[i]=scan.nextInt();
				if((W[i]+i)>max){
					max=(W[i]+i);
				}
			}
			System.out.println(max);
			
			
		}//while(testCase-->0)
		
	}
}//class