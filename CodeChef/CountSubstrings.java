import java.util.*;
class CountSubstrings{
	
	public static void main(String arg[]){
		
		Scanner scan=new Scanner(System.in);
		
		int testCase=scan.nextInt();
		while(testCase-- >0){
			
			long count=0;
			long nC2;
			long result;
			long LengthOfString=scan.nextInt();
			String str=scan.next();
			
			for(int i=0;i<LengthOfString;i++){
				
				if(str.charAt(i)=='1'){
					count=count+1;
				}
			}
			
			if(count!=0){
				nC2=(long)(count*(count-1))/2;
				result=(long)(nC2+count);
				System.out.println(result);
			}
			else
				System.out.println(count);
		}
	}
}