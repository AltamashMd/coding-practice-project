import java.util.*;
class subtractionGame1{
	public static  void main(String args[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		while(testCase-- >0){
			int i;
			int size=scan.nextInt();
			int arr[]=new int[size];
			for(i=0;i<size;i++){
				arr[i]=scan.nextInt();
			}//for()
			int k=arr[0];
			for(i=0;i<size;i++){
				k=gcd(arr[i],k);
			}//for()
				System.out.println(k);
		}//while()
	}//main()
	static int gcd(int a,int b){
		if(a==0)
			return b;
		else
			return gcd(b%a,a);
	}//gcd()
}//class