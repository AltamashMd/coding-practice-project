import java.util.Scanner;
class JhonyAndTheBeanstalk 
{ 	
	public static void main(String[] args) 
	{ 	
		Scanner scan=new Scanner(System.in);
		int test=scan.nextInt();
		while(test-- >0)
		{
			int flag=0;
			int onesCount=0;
			int N=scan.nextInt();
			int arr[]=new int[N];
			for(int i=0;i<N;i++)
			{
				arr[i]=scan.nextInt();
				if(arr[i]==1)
				{
					onesCount++;
				}
 			}
 			if(onesCount==N)
			{
 				System.out.println("Yes");
			}
			else
			{
					int i=0;
					int k=(N-1);
					while(i<N)
					{
						int count=0;
						int m=k;
						k--;
						for(int j=(N-1);j>=0;j--){
							if(arr[j]==m){
								if(m>0){
									m--;
								}
								count++;
							}
							else
							{
								break;
							}
						}
						if(count==N){
							System.out.println("Yes");
							flag=1;
							break;
						}
						i++;
					}
					if(flag==0)
					{
						System.out.println("No");
					}
			}
		}
	}
}
											
