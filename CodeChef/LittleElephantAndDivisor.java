import java.util.*;
class LittleElephantAndDivisor{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0){
			
			int N=scan.nextInt();
			int arr[]=new int[N];
			int flag=0;
			
			for(int i=0;i<N;i++){
				arr[i]=scan.nextInt();
			}
			
			int G=0;
			for(int i=0;i<N;i++){
			
				G=gcd(arr[i],G);
			}
				
			if(G!=1){
				
				for(int i=2;i<=Math.sqrt(G);i++){
				
					if(G%i==0){
						System.out.println(i);
						flag=1;
						break;
					}
				}
				if(flag!=1){
					System.out.println(G);
				}
			}
			else{
				System.out.println(-1);
			}
			
				
		}//while(testCase-->0)
	}//main()

	public static int gcd(int a,int b){
		
		if(b==0){
			return a;
		}
		else{
			return gcd(b,a%b);
		}
	}
}//class
