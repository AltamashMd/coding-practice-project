import java.util.Scanner;
class DistinctNumber{
	static StringBuffer sb=new StringBuffer();
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int i;
		while(testCase-->0){
			int size=scan.nextInt();
			int arr[]=new int[size];
			for(i=0;i<size;i++){
				arr[i]=scan.nextInt();
				int k=unique(arr[i]);
				if(k!=0)
					sb.append(k);
			}
			System.out.println(sb.length());
		}//while()
	}//main()
	static int unique(int x){
		int i=0;
		int flag=0;
		while(i<sb.length()){
			int n=Integer.parseInt(sb.substring(i,i+1));
			if(x==n){
				flag=1;
				break;
			}//if
		i++;
		}//while(i<sb.length())
		if(flag==0){
			return x;
		}
		else
			return 0;
	}//unique()
}//class
