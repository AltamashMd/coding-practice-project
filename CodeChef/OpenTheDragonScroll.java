import java.util.*;
class OpenTheDragonScroll{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		StringBuffer sbOfNum1=new StringBuffer();
		StringBuffer sbOfNum2=new StringBuffer();
		
		while(testCase-->0){
			int noOfDigit=scan.nextInt();
			int num1=scan.nextInt();
			int num2=scan.nextInt();
			
			int numberOf0sInNum1=0;
			int numberOf0sInNum2=0;
			int numberOf1sInNum1=0;
			int numberOf1sInNum2=0;
			
			String num1InBinary=Integer.toBinaryString(num1);
			String num2InBinary=Integer.toBinaryString(num2);
			
			sbOfNum1=new StringBuffer(num1InBinary);
			sbOfNum2=new StringBuffer(num2InBinary);
			
			if(num1InBinary.length()<noOfDigit){
				int temp=noOfDigit - num1InBinary.length();
				
				for(int i=0;i<temp;i++){
					sbOfNum1.insert(0,'0');
				}
			}
			
			if(num2InBinary.length()<noOfDigit){
				int temp=noOfDigit - num2InBinary.length();
				
				for(int i=0;i<temp;i++){
					sbOfNum2.insert(0,'0');
				}
			}
			for(int i=0;i<noOfDigit;i++){
				
				if(sbOfNum1.charAt(i)=='0'){
					numberOf0sInNum1++;
				}
				else{
					numberOf1sInNum1++;
				}
				
				if(sbOfNum2.charAt(i)=='0'){
					numberOf0sInNum2++;
				}
				else{
					numberOf1sInNum2++;
				}
			}
			
			int x=Math.min(numberOf1sInNum1,numberOf0sInNum2);
			int y=Math.min(numberOf0sInNum1,numberOf1sInNum2);
			int p=x+y;
			System.out.println(((1<<p)-1)<<(noOfDigit-p));
			
		}//while()
	}//main()
}//class
