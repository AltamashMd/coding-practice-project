import java.util.Scanner;
class BestBox{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		while(testCase-->0){
			int p=scan.nextInt();
			int s=scan.nextInt();
			double h=(p-(Math.sqrt(p*p-24*s)))/12;
			double a=((2*s)-(p*h)+(4*h*h))/4;
			double volume=(a*h);
			double roundoff=Math.round(volume*100.0)/100.0;
			System.out.println(roundoff);
		}//while()
	}//main()
}//class
