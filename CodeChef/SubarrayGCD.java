import java.util.*;
class SubarrayGCD
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		int result=0;
		
		
		
		while(testCase-->0)
		{
			int N=scan.nextInt();
			
			for(int i=0;i<N;i++){
				
				int input=scan.nextInt();
				
				result=(i==0) ? input : gcd(result,input);
			}
			
			if(result==1){
				
				System.out.println(N);
			}
			else{
				
				System.out.println(-1);
			}
			
			
		}//while(testCase-->0)
		
	}
	
	public static int gcd(int a,int b){
		
		return (b==0 ? a : gcd(b,a%b));
	}
	
	

}//class