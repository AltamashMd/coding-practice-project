import java.util.*;
class NotATriangle2
{
	public static void main(String[] arg)
	{
		Scanner scan=new Scanner(System.in);
		
			int N;
			while((N=scan.nextInt())!=0)
			{
				int result=0;
				int count=0;
				int a=0;
				int b=0;
				int c=0;
				int arr[]=new int[N];
				
				for(int i=0;i<N;i++)
				{
					arr[i]=scan.nextInt();
				}
				
				Arrays.sort(arr);
				
				if(N>3)
				{
					for(int i=0;i<=(N-3);i++)
					{
						for(int j=i+1;j<(N-1);j++)
						{
							count=BinarySearch(arr,arr[i],arr[j],j+1,N-1);
							result=result+count;
						}
					}
				
					System.out.println(result);
				}
				else
				{
					a=arr[0];
					b=arr[1];
					c=arr[2];
					if((a+b)<c)
					{
						System.out.println(1);
					}
					else
					{
						System.out.println(0);
					}
					
				}
			}
	}
	
	public static int BinarySearch(int arr[],int a,int b,int low,int high)
	{
		int count=0;
		int n=high;
		
		while(low<=high)
		{
			int flag=0;
			int mid=(low+high)/2;
			
			if(arr[mid]>(a+b))
			{
				flag=1;
				count =((n-mid)+1);
			}
			if(flag==0)
			{
				low=mid+1;
			}
			else
			{
				high=mid-1;
			}
		}
		return count;
	}
}