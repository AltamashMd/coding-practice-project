import java.util.*;
class TheMinimumNumberOfMoves{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0){
			int numberOfWorkers=scan.nextInt();
			int firstInput=scan.nextInt();
			int min=firstInput;
			int sum=firstInput;
			for(int i=0;i<numberOfWorkers-1;i++){
				int input=scan.nextInt();
				sum=sum+input;
				if(input<min){
					min=input;
				}
			}
			
			int noOfOperationRequired=(sum-numberOfWorkers*min);
			System.out.println(noOfOperationRequired);
			
		}//while(testCase-->0)
			
	}//main()
	
	
}//class
