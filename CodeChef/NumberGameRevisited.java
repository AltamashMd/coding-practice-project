import java.util.*;
class NumberGameRevisited{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		while(testCase-->0){
			int n=scan.nextInt();
			if(n%4==1)
					System.out.println("ALICE");
			else
				System.out.println("BOB");
		}//while()
	}//main()
}//class
