import java.util.*;
class TheArmy{
	public static void main(String arg[]){
		Scanner scan=new Scanner(System.in);
		int testCase=scan.nextInt();
		
		while(testCase-->0){
			
			int N=scan.nextInt();
			int M=scan.nextInt();
			
			int minArr[]=new int[N];
			int maxArr[]=new int[N];
			int positionArr[]=new int[M];
			
			for(int i=0;i<M;i++){
				positionArr[i]=scan.nextInt();
			}
			
			Arrays.sort(positionArr);
			
			int x=0;
			for(int i=positionArr[0];i<N;i++){
				minArr[i]=x++;
			}
			
			x=1;
			for(int i=positionArr[0]-1;i>=0;i--){
				minArr[i]=x++;
			}
			
			x=0;
			for(int i=positionArr[M-1];i<N;i++){
				maxArr[i]=x++;
			}
			
			x=1;
			for(int i=positionArr[M-1]-1;i>=0;i--){
				maxArr[i]=x++;
			}
			
			for(int i=0;i<N;i++){
					
				if(maxArr[i]>minArr[i])
					System.out.print(maxArr[i]+" ");
				else
					System.out.print(minArr[i]+" ");
			}
			System.out.println();
			
				
		}//while(testCase-->0)
	}//main()
}//class
