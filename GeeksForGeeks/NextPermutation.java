import java.util.*;
class NextPermutation
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				int N=input.nextInt();
				int arr[]=new int[N];
				
				for(int i=0;i<N;i++){
					
					arr[i]=input.nextInt();
				}
				
				for(int i=N-1;i>0;i--){
					
					if(arr[i]>arr[i-1]){
						swap(arr,i-1,N-1);
						sort(arr,i,N-1);
						break;
					}
				}
				
				for(int i=0;i<N;i++){
					
					System.out.print(arr[i]+" ");
				}
				
				System.out.println();
			}
			
			
	}
	
	public static void swap(int arr[],int start,int end){
		int diff=0;
		int min=100000;
		int pos=-1;
		for(int i=start+1;i<=end;i++){
			
			if((arr[i]-arr[start])>0){
				diff=(arr[i]-arr[start]);
			}
			
			if(diff<min){
				
				min=diff;
				pos=i;
			}
		}
		
		int temp=arr[start];
		arr[start]=arr[pos];
		arr[pos]=temp;
	}
	
	public static void sort(int arr[], int start, int end){
		
		for(int i=start;i<end;i++){
			for(int j=i+1;j<=end;j++){
				if(arr[i]>arr[j]){
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
	}
}