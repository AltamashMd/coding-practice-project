import java.util.*;

class FourElements
{
	
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testCase=input.nextInt();
			
			
			while(testCase-- >0)
			{
				int sizeOfArray=input.nextInt();
				
				int[] inputArray=new int[sizeOfArray];
				
				ArrayList<Integer> arrayOfPossible4Combination=new ArrayList<Integer>();
				int status=0;
				
				for(int index=0; index<sizeOfArray; index++){
					
					inputArray[index]=input.nextInt();
				}
				
				int expectedSum=input.nextInt();
				
				arrayOfPossible4Combination=fourBitNumber(sizeOfArray);
				
				for(int OuterIndex=0; OuterIndex<arrayOfPossible4Combination.size(); OuterIndex++){
					
					int sum=0;
					
					for(int innerIndex=0; innerIndex<sizeOfArray; innerIndex++){ 
						
						int x=(arrayOfPossible4Combination.get(OuterIndex) & (1<<innerIndex));
						
						if(x!=0){
							
							sum=sum + inputArray[innerIndex];
						}
					}
					if(sum==expectedSum){
						status=1;
						break;
					}
				}
				
				System.out.println(status);
			}
	}
	
	static ArrayList<Integer> fourBitNumber(int sizeOfArray){
		
		int totalCombination=(1<<sizeOfArray);
		
		ArrayList<Integer> arrayOfPossible4Combination=new ArrayList<Integer>();
		
		
		for(int index=15;index<totalCombination;index++){
			
			int count=countSetBits(index);
			
			if(count==4){
				
				arrayOfPossible4Combination.add(index);
				
			}
		}
		
		return arrayOfPossible4Combination;
	}
	
	static int countSetBits(int number) 
    { 
        int count = 0; 
        while (number > 0) 
        { 
            number = number & (number - 1) ; 
            count++; 
        } 
        return count; 
    } 
	
	
}