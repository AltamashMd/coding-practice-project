import java.util.*;
import java.lang.*;
import java.io.*;
import java.math.*;

 class Pair{
     int first;
     int second;
	 int sum;
 }
class FourElementsPairwise
 {
    public static boolean get4Numbers(int a[],int sums)
	{
		 
		 int len=a.length;
		
		 List<Pair> pairList=new ArrayList<Pair>();
		 
		 for(int i=0;i<len-1;i++){
			 
			 for(int j=i+1;j<len;j++){
				 
				 Pair pair=new Pair();
				 pair.sum=a[i]+a[j];
				 pair.first=i;
				 pair.second=j;
				 
				 pairList.add(pair);
			 }
		 }
		 
		 pairList.sort((Pair p1, Pair p2)->(p1.sum - p2.sum));
		 
		 
		 int i=0;
		 int j=pairList.size()-1;
		 int n=pairList.size();
		 
		 while(i<n && j>=0){
			 
			 if(((pairList.get(i).sum + pairList.get(j).sum)==sums) && noCommon(pairList.get(i),pairList.get(j))){
				 return true;
			 }
			 else if((pairList.get(i).sum + pairList.get(j).sum)<sums)
				 i++;
			 else
				 j--;
		 }
		 
		 return false;
	}
	 
	 public static boolean noCommon(Pair p1, Pair p2){
		 
		 if(p1.first==p2.first || p1.first==p2.second || p1.second==p2.first || p1.second==p2.second)
			 return false;
		 
		 return true;
	 }		 
     
     
	public static void main(String[] args)
	{
	    Scanner sc = new Scanner(System.in);
	    int testCases = sc.nextInt();
	    for(int z = 0 ; z < testCases;z++)
	    {
	       int size = sc.nextInt();
	       int ar[] = new int[size];
	       for(int i=0;i<size;i++)
	       {
	           ar[i] =sc.nextInt();
	       }
	       int sum = sc.nextInt();
	       System.out.println(get4Numbers( ar,sum)==true?1:0);
	    }
	}
}