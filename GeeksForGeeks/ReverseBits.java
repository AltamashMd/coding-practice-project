import java.util.*;
class ReverseBits
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				int N=input.nextInt();
				long sum=0;
				String binaryString=String.format("%32s", Integer.toBinaryString(N)).replace(" ", "0");
				
				for(int i=31;i>=0;i--)
				{
					if(binaryString.charAt(i)=='1')
					{
						sum=(long)(sum + Math.pow(2,i));
					}
					
				}
				
				System.out.println(sum);
			}
	}
	
	
}