import java.util.*;

class ChocolateStation
{
	
	public static void main(String[] arg)
	{
		Scanner scan =  new Scanner(System.in); 
		
			int testCase=scan.nextInt();
			
			
			while(testCase-- >0)
			{
				int noOfStation=scan.nextInt();
				int[] arrayOfChocolates=new int[noOfStation];
				
				
				for(int i=0;i<noOfStation;i++){
					
					arrayOfChocolates[i]=scan.nextInt();
				}
				
				int costOfChocolate=scan.nextInt();
				
				int buy=arrayOfChocolates[0];
				int balance=0;
				
				for(int i=0;i<noOfStation-1;i++){
					
					if((arrayOfChocolates[i]-arrayOfChocolates[i+1])>0){
						
						balance = balance + (arrayOfChocolates[i]-arrayOfChocolates[i+1]);
					}
					else{
						
						if( ( balance -(arrayOfChocolates[i+1]-arrayOfChocolates[i]))>0){
							balance = balance - (arrayOfChocolates[i+1]-arrayOfChocolates[i]);
						}
						else{
							
							buy = buy +  ((arrayOfChocolates[i+1]-arrayOfChocolates[i]) - balance);
							balance=0;
						}
						
					}
				}
				
					System.out.println(buy*costOfChocolate);
			}
	}
}