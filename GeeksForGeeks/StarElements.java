import java.util.*;
import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 

class StarElements
{
	
	public static void main(String[] arg) throws IOException
	{
		Scanner scan =  new Scanner(System.in); 
		
			int testCase=scan.nextInt();
			
			
			while(testCase-- >0)
			{
				int sizeOfArray=scan.nextInt();
				int[] arrayOfElements=new int[sizeOfArray];
				
				int max=0;
				int temp=0;
				
				for(int i=0;i<sizeOfArray;i++){
					
					arrayOfElements[i]=scan.nextInt();
					
					if(arrayOfElements[i]>=max){
						
						if(arrayOfElements[i]==max){
							temp=arrayOfElements[i];
							max=arrayOfElements[i];
						}
						else
							max=arrayOfElements[i];
					}
				}
				
				for(int i=0;i<sizeOfArray;i++){
					
					boolean status=isStarElement(arrayOfElements,i);
					
					if(status){
						System.out.print(arrayOfElements[i]+" ");
					}
				}
				System.out.println();
				
				if(max!=temp)
					System.out.println(max);
				else
					System.out.println(-1);
			}
	}
	
	public static boolean isStarElement(int[] arrayOfElements,int position){
		
		int starElement=arrayOfElements[position];
		
		for(int i=position+1;i<arrayOfElements.length;i++){
			
			if(arrayOfElements[i]>=starElement){
				return false;
			}
		}
		
		return true;
	}
	
	
}