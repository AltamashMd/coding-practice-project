import java.util.*;
import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 

class PairsWithPositiveNegativeValues
{
	
	public static void main(String[] arg) throws IOException
	{
		Scanner scan =  new Scanner(System.in); 
		
			int testCase=scan.nextInt();
			
			
			while(testCase-- >0)
			{	
				int arraySize=scan.nextInt();
				
				List<Integer> positiveList=new ArrayList<>();
				List<Integer> negativeList=new ArrayList<>();
				
				for(int i=0;i<arraySize;i++){
					
					int input=scan.nextInt();
					
					if(input<0)
						negativeList.add(input);
					else
						positiveList.add(input);
				}
				
				/*System.out.println("List of negative integer");
				for(Integer x : negativeList){
					System.out.print(x+" ");
				}
				
				
				System.out.println();
				
				
				System.out.println("List of positive integer");
				
				for(Integer x : positiveList){
					System.out.print(x+" ");
				}
				
				System.out.println();
				*/
				
				Collections.sort(negativeList);
				Collections.sort(positiveList);
				
				/*System.out.println("List of negative integer after sorting");
				for(Integer x : negativeList){
					System.out.print(x+" ");
				}
				
				System.out.println();
				
				*/
				for(int i=negativeList.size()-1;i>=0;i--){
					
					int searchInteger=negativeList.get(i)*(-1);
					/*System.out.println("searchInteger "+searchInteger);
					
					System.out.println("size of positive list "+positiveList.size());
					
					for(int k=0;k<positiveList.size();k++){
						
						System.out.println("size of positive list "+positiveList.size());
					
						if((negativeList.get(i)+positiveList.get(k))==0){
								System.out.print(negativeList.get(i)+" "+positiveList.get(k)+" ");
								positiveList.remove(k);
								break;
						}
					}*/
					
					int output=BinarySearch(searchInteger,positiveList);
					
					if(output!=0){
						
						System.out.print(negativeList.get(i)+" "+output+" ");
						positiveList.remove(new Integer(output));
					}
					
					
				}
				
				System.out.println();
			}
	}
	
	public static int BinarySearch(int searchInteger, List<Integer> positiveList){
		
		int low=0;
		int high=positiveList.size()-1;
		
		while(low<=high){
			
			int mid=(low+high)/2;
			
			if(searchInteger==positiveList.get(mid)){
				
				return positiveList.get(mid);
			}
			else if(searchInteger>positiveList.get(mid)){
				low=mid+1;
			}
			else{
				high=mid-1;
			}
		}
		
		return 0;
	}
}