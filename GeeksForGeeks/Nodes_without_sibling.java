import java.util.*;
import java.lang.*;
import java.io.*;
class Node
{
    int data;
    Node left, right;
    Node(int item)
    {
        data = item;
        left = right = null;
    }
}
class Nodes_without_sibling
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t > 0)
        {
            HashMap<Integer, Node> m = new HashMap<Integer, Node> ();
            int n = sc.nextInt();
            Node root = null;
            while (n > 0)
            {
                int n1 = sc.nextInt();
                int n2 = sc.nextInt();
                char lr = sc.next().charAt(0);
                //  cout << n1 << " " << n2 << " " << (char)lr << endl;
                Node parent = m.get(n1);
                if (parent == null)
                {
                    parent = new Node(n1);
                    m.put(n1, parent);
                    if (root == null)
                        root = parent;
                }
                Node child = new Node(n2);
                if (lr == 'L')
                    parent.left = child;
                else
                    parent.right = child;
                m.put(n2, child);
                n--;
            }
            GfG g = new GfG();
            g.printSibling(root);
            System.out.println();
            t--;
			
        }
    }
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

//User function Template for Java
/*  A Binary Tree node
class Node
{
    int data;
    Node left, right;
    Node(int item)
    {
        data = item;
        left = right = null;
    }
}
*/
class GfG
{
	List<Integer> listOfNode=new ArrayList<Integer>();
	void printSibling(Node node)
	{
		//System.out.println("Entering into printSibling.....");
	    
		printSiblingUtil(node);
			//System.out.println("Before Printing......flag value "+flag);
			Collections.sort(listOfNode);
		
			for(Integer x : listOfNode){
				//System.out.println("Inside for....");
				System.out.print(x+" ");
			}	
			//System.out.println("After Printing...... flag value "+flag);
		
		//System.out.println("Exiting from printSibling.....");
		
	}
	
	private void printSiblingUtil(Node node){
		if(node==null)
		    return;
		if(node.left!=null && node.right==null){
			//System.out.println("adding node into list "+node.left.data);
		    listOfNode.add(node.left.data);
		}
		else if(node.left==null && node.right!=null){
			//System.out.println("adding node into list "+node.right.data);
		     listOfNode.add(node.right.data);
		}
		
		printSiblingUtil(node.left);
		printSiblingUtil(node.right);
		
	}
}