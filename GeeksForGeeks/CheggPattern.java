import java.util.*;
class CheggPattern
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			System.out.println("Enter the no. of test case");
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				System.out.println("Enter the choice : \n Press 1 for rectangle pattern \n Press 2 for triangle pattern \n Press 3 for both");
				
				int choice = input.nextInt();
				
				if(choice==1){
					
					for(int i=0;i<10;i++){
						for(int j=0;j<10;j++){
							if(j==0 || j==9){
								System.out.print("@");
							}
							else{
								if(i==0 || i==9){
									System.out.print("@");
								}
								else{
									System.out.print(" ");
								}
							}
						}
						System.out.println();
					}
				}
				else if (choice==2){
					int temp=6;
					int even=2;
					for(int i=0;i<5;i++){
						
						for(int j=0;j<temp;j++){
							
							if(j==(temp-1)){
								System.out.print("%");
							}
							else{
								System.out.print(" ");
							}
						}
						
						if(i>0){
							
							for(int k=0;k<even;k++){
								
								if(k==even-1){
									System.out.print("%");
								}
								else{
									System.out.print(" ");
								}
							}
							
							even=even+2;
						}
						
						temp--;
						System.out.println();
					}
					
					for(int p=0;p<11;p++){
						System.out.print("%");
					}
					System.out.println();
				}
				else if(choice==3){
					
					for(int i=0;i<10;i++){
						for(int j=0;j<10;j++){
							if(j==0 || j==9){
								System.out.print("@");
							}
							else{
								if(i==0 || i==9){
									System.out.print("@");
								}
								else{
									System.out.print(" ");
								}
							}
						}
						System.out.println();
					}
					
					System.out.println();
					System.out.println();
					
					int temp=6;
					int even=2;
					for(int i=0;i<5;i++){
						
						for(int j=0;j<temp;j++){
							
							if(j==(temp-1)){
								System.out.print("%");
							}
							else{
								System.out.print(" ");
							}
						}
						
						if(i>0){
							
							for(int k=0;k<even;k++){
								
								if(k==even-1){
									System.out.print("%");
								}
								else{
									System.out.print(" ");
								}
							}
							
							even=even+2;
						}
						
						temp--;
						System.out.println();
					}
					
					for(int p=0;p<11;p++){
						System.out.print("%");
					}
				}
				else{
					
					System.out.println("Invalid choice");
				}
			}
	}
}