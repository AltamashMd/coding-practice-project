import java.util.*;

class SumOfLengthsOfNonOverlappingSubArrays
{
	
	public static void main(String[] arg)
	{
		Scanner scan =  new Scanner(System.in); 
		
			int testCase=scan.nextInt();
			
			
			while(testCase-- >0)
			{
				int noOfElements=scan.nextInt();
				int[] arrayOfElements=new int[noOfElements];
				
				int sum=0;
				
				
				for(int i=0;i<noOfElements;i++){
					
					arrayOfElements[i]=scan.nextInt();
				}
				
				int maxElement=scan.nextInt();
				
				for(int i=0;i<noOfElements;){
					
					if(arrayOfElements[i]<=maxElement){
						int j=i;
						while(i<noOfElements && arrayOfElements[i]<=maxElement){
							i++;
						}
						System.out.println("i "+i+" j "+j);
						int lengthOfSubarray=NonOverlappingSubArray(j,i,arrayOfElements,maxElement);
						System.out.println("lengthOfSubarray "+lengthOfSubarray);
						
						sum = sum + lengthOfSubarray;
						
					}
					i++;
				}
				
					System.out.println(sum);
			}
	}
	
	public static int NonOverlappingSubArray(int start, int end, int[] arrayOfElements, int maxElement){
		
		for(int i=start;i<end;i++){
			
			if(arrayOfElements[i]==maxElement){
				return (end-start);
			}
		}
		
		return 0;
			
	}
}