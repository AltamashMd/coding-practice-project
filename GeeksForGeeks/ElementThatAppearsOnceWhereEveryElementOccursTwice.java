import java.util.*;

class ElementThatAppearsOnceWhereEveryElementOccursTwice
{
	
	public static void main(String[] arg)
	{
		Scanner scan =  new Scanner(System.in); 
		
			int testCase=scan.nextInt();
			
			
			while(testCase-- >0)
			{
				int noOfElements=scan.nextInt();
				int[] arrayOfElements=new int[noOfElements];
				
				
				for(int i=0;i<noOfElements;i++){
					
					arrayOfElements[i]=scan.nextInt();
				}
				
				Arrays.sort(arrayOfElements);
				
				if(noOfElements==1){
					
					System.out.println(arrayOfElements[0]);
					
				}
				
				else{
				
					for(int i=0;i<noOfElements;){
					
						if(i<noOfElements-1 && (arrayOfElements[i]==arrayOfElements[i+1])){
							i=i+2;
						}
						else if(i<noOfElements-1 && arrayOfElements[i]!=arrayOfElements[i+1]){
							
								System.out.println(arrayOfElements[i]);
								break;
						}
						else if(i==noOfElements-1){
							System.out.println(arrayOfElements[i]);
							break;
						}
					}
				}
			}
	}
	
}