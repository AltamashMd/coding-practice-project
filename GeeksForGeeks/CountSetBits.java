import java.util.*;
class CountSetBits
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				int N = input.nextInt();
				
				int count = 0;
				System.out.println(Integer.toBinaryString(N));
				
				for(count=0; N !=0 ; N >>= 1){
					
					if( (N & 1)==1){
						count++;
					}
				}
					
					System.out.println(count);
			}
	}
}