import java.util.*;
class ChangeAllEvenBitsInANumberToZero
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				int N=input.nextInt();
				String binaryString=Integer.toBinaryString(N);
				StringBuffer sb=new StringBuffer(binaryString);
				
				sb.reverse();
				
				for(int i=0;i<sb.length();i++)
				{
					if(i%2==0 && sb.charAt(i)=='1')
					{
						sb.setCharAt(i,'0');
					}
					
				}
				sb.reverse();
				
				int result =Integer.parseInt(sb.toString(),2);
				
				System.out.println(result);
			}
	}
	
	
}