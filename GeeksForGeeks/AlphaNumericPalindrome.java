import java.util.*;
import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 

class AlphaNumericPalindrome
{
	
	public static void main(String[] arg) throws IOException
	{
		BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in)); 
		
			int testCase=Integer.parseInt(reader.readLine());
			
			
			while(testCase-- >0)
			{
				int flag=0;
          
				String InputString = reader.readLine(); 

				
				int InputStringLenght=InputString.length();
				
				
				int i=0;
				int j=InputStringLenght-1;
				
				
				while(i<j){
					
					if(Character.isAlphabetic(InputString.charAt(i)) || Character.isDigit(InputString.charAt(i))){
						
						if(Character.isAlphabetic(InputString.charAt(j)) || Character.isDigit(InputString.charAt(j))){
							
							if(Character.toLowerCase(InputString.charAt(i))==Character.toLowerCase(InputString.charAt(j))){
								i++;
								j--;
							}
							else{
								
								flag=1;
								break;
							}
							
						}
						else
							j--;
					}
					else
						i++;
				}
				
				if(flag==0){
					System.out.println("YES");
				}
				else if (flag==1){
					System.out.println("NO");
				}
				
			}
	}
}