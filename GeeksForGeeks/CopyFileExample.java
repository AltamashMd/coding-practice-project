import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
 
public class CopyFileExample 
{
	
	CopyFileExample(File readingFile, File)
    public static void main(String[] args)
    {	
	
 
    	try{
    	    File infile =new File("C:\\Users\\HP\\Desktop\\MyInputFile.txt");
    	    File outfile =new File("C:\\Users\\HP\\Desktop\\MyOutputFile.txt");
 
    	    FileReader inReader = new FileReader(infile);
			BufferedReader bufferReader = new BufferedReader(inReader);
			
    	    PrintWriter outWriter = new PrintWriter(outfile);
 
			
			String eachLine = bufferReader.readLine();
			
			while(eachLine!=null){
				
				outWriter.println(eachLine.toUpperCase());
				
				eachLine=bufferReader.readLine();
			}
 
    	    

    	    //Closing the input/output file streams
    	    bufferReader.close();
    	    outWriter.close();

    	    System.out.println("File copied successfully!!");
 
    	}catch(IOException ioe){
    		ioe.printStackTrace();
    	 }
    }
}