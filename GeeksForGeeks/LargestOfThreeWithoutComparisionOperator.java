import java.util.*;
class LargestOfThreeWithoutComparisionOperator
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				int a=input.nextInt();
				int b=input.nextInt();
				int c=input.nextInt();
				
				if((a/b)!=0)
				{
					if((a/c)!=0)
					{
						System.out.println("largest number is  a : " +a);
					}
					else
					{
						System.out.println("largest number is c : "+c);
					}
					
				}
				else
				{
					if((b/c)!=0)
					{
						System.out.println("largest number is b : "+b);
					}
					else
					{
						System.out.println("largest number is c : "+c);
					}
				}
			}
	}
	
	
}