import java.util.*;
class Chegg
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				String str=input.next();
				int count=0;
				
				Set<Character> distinctCharacter=new HashSet<Character>();
				
				for(int i=0;i<str.length();i++){
					
					if(Character.isAlphabetic(str.charAt(i))){
						
						distinctCharacter.add(Character.valueOf(str.charAt(i)));
						
					}
					
					
				}
				
				List<StringBuffer> words=new ArrayList<StringBuffer>();
				
				for(Character c : distinctCharacter){
					count=0;
					
					char ch=c;
					
					for(int i=0;i<str.length();i++){
						
						if(str.charAt(i)==ch){
							count++;
						}
						
					}
					
					int k=0;
					
					for(k=0;k<str.length()-1;){
						
						
						if(k+1<str.length() && str.charAt(k)==ch && str.charAt(k+1)==ch){
							
							StringBuffer word=new StringBuffer();
							
							word.append(str.charAt(k));
							k++;
							word.append(str.charAt(k));
							while(k+1 < str.length() && str.charAt(k)==str.charAt(k+1)){
								
								word.append(str.charAt(k+1));
								k++;
							}
							
							words.add(word);
						}
						else
							k++;
							
							
					}
					
					System.out.println(c+" "+count);
					
				}
				
				Set<String> distinctWord=new HashSet<String>();
				
				for(StringBuffer sb : words){
					
					distinctWord.add(sb.toString());
				}

				
				for(String word : distinctWord){
					
					
					int wordCount=0;
					for(int i=0;i<words.size();i++){
						
			
						
						if(word.equals((words.get(i)).toString())){
				
							wordCount++;
							
						}
					}
					
					System.out.println(word + " " + wordCount);
				}
				
				List<StringBuffer> digits=new ArrayList<StringBuffer>();
				for(int i=0;i<str.length();){
					
					if(Character.isDigit(str.charAt(i))){
						
						StringBuffer digit=new StringBuffer();
						
						
						while(Character.isDigit(str.charAt(i))){
							digit.append(str.charAt(i));
							i++;
						}
						
						digits.add(digit);
						
					}
					else
						i++;
				}
				
				for(StringBuffer digi : digits){
					
					System.out.println(digi);
				}
				
				
				
				
				
				
			}
	}
}