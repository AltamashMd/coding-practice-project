import java.util.*;
import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 

class PrimeNumberOfSetBits
{
	
	public static void main(String[] arg) throws IOException
	{
		Scanner scan =  new Scanner(System.in); 
		
			int testCase=scan.nextInt();
			
			
			while(testCase-- >0)
			{
				int count=0;
				int L=scan.nextInt();
				int R=scan.nextInt();
				
				for(int i=L;i<=R;i++){
					
					System.out.println("input "+i);
					
					int countOfSetBits=CountSetBits(i);
					
					System.out.println("set bits of "+i+" is "+countOfSetBits);
					
					System.out.println("isPrime() returns "+isPrime(countOfSetBits));
					
					if(isPrime(countOfSetBits)){
						count++;
					}
				}
				System.out.println("Result is "+count);
			}
	}
	
	public static int CountSetBits(int number){
		
		int countOfBits=0;
		
		while(number>0){
			
			number=number& (number-1);
			
			countOfBits++;
		}
		
		return countOfBits;
	}
	
	public static boolean isPrime(int number){
		
		int flag=0;
		
		for(int i=2;i<=Math.sqrt(number);i++){
			
			if(number%i==0){
				flag=1;
				break;
			}
		}
		
		if(flag==0 && number>1)
			return true;
		else
			return false;
	}
}