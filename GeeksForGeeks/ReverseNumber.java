import java.util.*;
class ReverseNumber
{
	public static void main(String[] arg)
	{
		Scanner input=new Scanner(System.in);
		
			int testcase=input.nextInt();
			while(testcase-- >0)
			{
				int N = input.nextInt();
				
				int reverseNumber = 0;
				
				while(N !=0){
					
					reverseNumber = reverseNumber*10 + N%10;
					
					N /= 10;
				}
					
					System.out.println(reverseNumber);
			}
	}
}