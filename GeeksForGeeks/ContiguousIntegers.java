import java.util.*;
import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 

class ContiguousIntegers
{
	
	public static void main(String[] arg) throws IOException
	{
		Scanner scan =  new Scanner(System.in); 
		
			int testCase=scan.nextInt();
			
			
			while(testCase-- >0)
			{
				int flag=0;
          
				int arraySize=scan.nextInt();
				
				TreeSet<Integer> set=new TreeSet<Integer>();
				
				for(int i=0;i<arraySize;i++){
					
					Integer input=scan.nextInt();
					set.add(input);
				}
				
				int flag1=0;
				Integer first=0;
				Integer second=0;
				for(Integer x:set){
					
					if(flag1==0){
						first=x;
						flag1=1;
					}
					else{
						second=x;
						if((second-first)!=1){
							flag=1;
							break;
						}
						first=second;
					}
					
				}
				
				
				if(flag==0)
					System.out.println("Yes");
				else
					System.out.println("No");
				
			}
	}
}