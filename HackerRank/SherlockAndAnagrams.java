import java.util.*;

public class SherlockAndAnagrams{

    public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		
		int testcase = scanner.nextInt();
		
		while(testcase-- > 0){
				
			String str = scanner.next();
			
			System.out.println(sherlockAndAnagrams(str));
		}
		

    }
	
	public  static int sherlockAndAnagrams(String s) {
		
		int result = 0;
		int sum1=0;
		int sum2=0;
		int m=0;
		int n=0;
		int tempCount=0;
		 
	
		for(int count=1;count<s.length();count++){

			for(int i=0;i<s.length()-1;i++){
		 
				for(int j=i+1;j<s.length();j++){
			 
					sum1 = 0;
					sum2 = 0;
					m=i;
					n=j;
					tempCount=0;
					for(int k=0;k<count;k++){
				 
						if(m < s.length() && n < s.length()){
							
							sum1 += s.charAt(m++);
							sum2 += s.charAt(n++);
							tempCount++;
						}
					}
					
					//System.out.println("sum1 : "+sum1);
					//System.out.println("sum2 : "+sum2);
					
					if(sum1 == sum2 && count==tempCount){
						
						//System.out.println("sum1==sum2");
						//System.out.println("sum1 is : "+sum1);
						//System.out.println("sum2 is : "+sum2);
						
						result++;
					}
					
					if(m==s.length()-1 || n==s.length()){
						break;
					}

				}
				
				if(count == s.length()-1)
					break;
			}
			
		}
		
		return result;

    }

}
