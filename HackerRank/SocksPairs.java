import java.util.*;
public class SocksPairs{
	
	public static void main(String[] args){
		
		Scanner scan = new Scanner(System.in);
		
		int n = scan.nextInt();
		
		HashMap<Integer, Integer> colors = new HashMap<Integer, Integer>();

		
		while(n-- >0){
			
			
			int c = scan.nextInt();
			
			Integer frequency = colors.get(c);
			System.out.println(frequency);
			
			if(frequency==null){
				colors.put(c,1);
			}
			else{
				
				colors.put(c,frequency+1);
			}
		}
		
		int sum=0;
		for(Integer i : colors.values()){
			
			sum += i/2;
			System.out.print(i+" ");
		}
		System.out.println();

		System.out.print("Sum "+sum);

	}

}