import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class RepeatedStringSolution {

    
    static long repeatedString(String s, long n) {
		
		long countOfa1=0,countOfa2=0;
		//String s1=s;
		
		long repeatTimes = n/s.length();
		System.out.println("repeatTimes : "+repeatTimes);
		
		for(int i=0;i<s.length();i++){
			
			if(s.charAt(i)=='a'){
				countOfa1++;
			}
		}

		long remainder = n % s.length();
		System.out.println("remainder : "+remainder);
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(s.substring(0,(int)remainder));
		
		//s += s1.substring(0,(int)remainder);
		
		System.out.println("final string  : "+sb);
		
		for(int i=0;i<sb.length();i++){
			
			if(sb.charAt(i)=='a'){
				countOfa2++;
			}
		}

		return countOfa1*repeatTimes + countOfa2;
		

    }

    public static void main(String[] args) throws IOException {

		Scanner scanner = new Scanner(System.in);
		String s =scanner.next();
		long n = scanner.nextLong();
		
        long result = repeatedString(s,n);
		
		System.out.println("result : "+result);

    }
}
