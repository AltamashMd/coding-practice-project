import java.util.*;
public class CountValleys{
	
	
	public static void main(String[] args){
		
		Scanner scan = new Scanner(System.in);
		
		int n = scan.nextInt();
		String s=scan.next();
		
		System.out.println(countingValleys(n,s));
	}


	public static int countingValleys(int n, String s) {

       int level=0;
	   int countOfValley=0;
	   
	   for(int i=0;i<n;i++){
		   
		   if(s.charAt(i)=='U'){
			   
			   if(level==-1){
				   countOfValley++;
			   }
			   level++;
		   }
		   else if(s.charAt(i)=='D'){
			   level--;
		   }
	   }
	   
	   return countOfValley;
    }
	
}