import java.util.*;

public class CheckMagazine {

   static void checkMagazine(String[] magazine, String[] note) {

		HashMap<String,Integer> magazineMap = new HashMap<String,Integer>();
		
		int count = 1;
		
		for(int i=0;i<magazine.length;i++){
			
			count = 1;
			
			if(magazineMap.get(magazine[i])!=null){
				
				magazineMap.put(magazine[i],magazineMap.get(magazine[i])+1);

			}
			else{
				
				magazineMap.put(magazine[i],count);
			}
			
		}
		
		for(int i=0;i<note.length;i++){
			
			System.out.println("Before removal");

			System.out.println(magazineMap);

			if(magazineMap.containsKey(note[i])){
				
				if(magazineMap.get(note[i])>0){
					
					magazineMap.replace(note[i],magazineMap.get(note[i])-1);
					//magazineMap.remove(i);
					//System.out.println("After removal");
				}
				else{
					System.out.println("No");
					return;
				}
				
				System.out.println(magazineMap);
			}
			else{
				System.out.println("No");
				return;
			}
		}
		System.out.println("Yes");

    }
    public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		
		int testcase = scanner.nextInt();
		
		while(testcase-- > 0){
				
			int m = scanner.nextInt();
			
			int n = scanner.nextInt();
			
			String[] magazine = new String[m];
			
			String[] note = new String[n];
			
			for(int i=0;i<m;i++){
				
				magazine[i] = scanner.next();
			}
			
			for(int i=0;i<n;i++){
				
				note[i] = scanner.next();
			}
			
			checkMagazine(magazine,note);
		}
		

    }
}
