import java.util.*;

public class MinimumSwaps2 {

    static int minimumSwaps1(int[] arr) {
		
		int max=0;
		int swapIndex=0;
		int diff=0;
		int swapCount=0;
		int arraySize = arr.length;
		int correctArrayPosition=0;
		
		for(int i=0;i<arraySize-1;i++){
			
			max=0;
			correctArrayPosition=i+1;
			
			for(int j=i+1;j<arraySize;j++){
				
				if(arr[i]==correctArrayPosition){
					break;
				}
				
				diff = arr[i]-arr[j];
				
				if(diff>max){
					
					max=diff;
					swapIndex=j;
				}
			}
			
			if(max>0){
				int temp = arr[i];
				arr[i] = arr[swapIndex];
				arr[swapIndex] = temp;
				
				swapCount++;
			}
			
		}
		
		for(int i=0;i<arraySize;i++){
			System.out.print(arr[i]+" ");
		}
		
		System.out.println();
		
		return swapCount;
    }
	
	static int minimumSwaps2(int[] arr) {
		
		int arrSize = arr.length;
		
		int swapCount = 0;
		
		for(int i=0;i<arrSize;i++){
		
			while(arr[i]!=i+1){
			
				swap(arr,arr[i],i);
				swapCount++;
			}
		}
		
		return swapCount;
    }
	
	public static void swap(int[] arr, int i, int j){
		
		int temp = arr[i-1];
		arr[i-1]=i;
		arr[j]=temp;
	}



    public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		
		int testcase = scanner.nextInt();
		
		while(testcase-- > 0){
				
				int n = scanner.nextInt();
				
				int[] arr = new int[n];
				
				for(int i=0;i<n;i++){
					arr[i]=scanner.nextInt();
				}
				
				//int minSwap = minimumSwaps1(arr);
				int minSwap = minimumSwaps2(arr);

				
				System.out.println(minSwap);
		}
		

    }
}
