import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class HourglassSumSolution {

    
    static int hourglassSum(int[][] arr) {
		
		int sum=0;
		int max=-64;
		
		for(int i=0;i<4;i++){
			
			sum=0;
			
			for(int j=0;j<4;j++){
				
				sum = arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2];
				
				//System.out.println("sum : "+sum);
				
				if(sum>max){
					max=sum;
				}
			}
			
		}

		return max;
		
    }

    public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);


		int[][] arr = new int[6][6];
		
		for(int i=0;i<6;i++){
			
			for(int j=0;j<6;j++){
				
				arr[i][j]=scanner.nextInt();
			}
		}
		
        int result = hourglassSum(arr);
		
		System.out.println("result : "+result);

    }
}
