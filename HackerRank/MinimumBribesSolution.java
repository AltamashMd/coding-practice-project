import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class MinimumBribesSolution {

    static int minBribe=0;
    static void minimumBribes(int[] q) {
		
		int minBribe=0;
		
		int max=Integer.MAX_VALUE;
		int mid=Integer.MAX_VALUE;
		int min=Integer.MAX_VALUE;
		
		for(int i=q.length-1;i>=0;i--){
				
			if(q[i]-(i+1)>2){
				System.out.println("Too chaotic");
				return;
			}
			else{
				
				if(q[i]>max){
					System.out.println("Too chaotic");
					return;
				}
				else if(q[i]>mid){
					minBribe = minBribe + 2;
				}
				else if(q[i]>min){
					minBribe = minBribe + 1;
				}
				
			}
			
			if(q[i]<min){
				max=mid;
				mid=min;
				min=q[i];
			}
			else if(q[i]<mid){
				max=mid;
				mid=q[i];
			}
			else if(q[i]<max){
				max=q[i];
			}
		}
		
		System.out.println(minBribe);

    }


    public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);
		
		
		int testcase = scanner.nextInt();
		
		while(testcase-- > 0){
			
			minBribe=0;
			
			int n = scanner.nextInt();
		
			int[] q = new int[n];
		
			for(int i=0;i<n;i++){
				q[i] = scanner.nextInt();
			}
			
			minimumBribes(q);
			
			/*for(int i=0;i<n;i++){
				System.out.print(q[i]+" ");
			}
			
			System.out.println();*/
			
		}
		
		

    }
}
