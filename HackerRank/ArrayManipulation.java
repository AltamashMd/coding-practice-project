import java.util.*;

public class ArrayManipulation {

   static long arrayManipulation(int n, int[][] queries) {
		
		long[] diff = new long[n+1];
		long[] arr = new long[n];
		
		for(int i=0;i<queries.length;i++){
				
			int a = queries[i][0];
			int b = queries[i][1];
			int k = queries[i][2];
				
			update(diff,a-1,b-1,k);
				
		}
		
		return printArray(arr,diff);

    }
	
	public static void update(long[] diff, int l, int r, int x){
		
		diff[l] += x;
		diff[r+1] -= x;
	}
	
	public static long printArray(long[] arr, long[] diff){
			
			long max = 0;
			
			for(int i=0;i<arr.length;i++){
				
				if(i==0){
					arr[0] = diff[0];
				}
				else{
					
					arr[i] = diff[i] + arr[i-1];
					
					if(arr[i]>max){
						
						max = arr[i];
					}
				}
			}
			return max;
		}
    public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		
		int testcase = scanner.nextInt();
		
		while(testcase-- > 0){
				
			int n = scanner.nextInt();
			
			int m = scanner.nextInt();
			
			int[][] queries = new int[m][m];
			
			for(int i=0;i<m;i++){
				
				for(int j=0;j<m;j++){
					queries[i][j]= scanner.nextInt();
				}
			}
			
			long result = arrayManipulation(n,queries);

				
			System.out.println(result);
		}
		

    }
}
