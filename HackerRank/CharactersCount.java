import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class CharactersCount {

   
    public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		String sentence = scanner.nextLine();
		
		characterSpaceVowelWordCounter(sentence);
    }
	
	public static void characterSpaceVowelWordCounter(String sentence){
		
		int characterCount=0;
		int spaceCount=0;
		int vowelCount=0;
		int wordCount=0;
		String result="";
		
		for(int i=0;i<sentence.length();i++){
			
			if(sentence.charAt(i)!=' '){
				characterCount++;
			}
			
			if(sentence.charAt(i)==' '){
				spaceCount++;
			}
			
			if(sentence.charAt(i)=='a' || sentence.charAt(i)=='e' || sentence.charAt(i)=='i' || sentence.charAt(i)=='o' || sentence.charAt(i)=='u' || sentence.charAt(i)=='A' || sentence.charAt(i)=='E' || sentence.charAt(i)=='I' || sentence.charAt(i)=='O' || sentence.charAt(i)=='U'){
				vowelCount++;
			}
			
		}
		
		String[] words = sentence.split(" ");
		
		for(String s : words){
			
			if(!s.isEmpty()){
				wordCount++;
			}
			System.out.println(s+" "+s.isEmpty());
		}
		
		System.out.print("Characters:"+characterCount+"Spaces:"+spaceCount+"Vowels:"+vowelCount+"Words:"+wordCount);
	}
}
