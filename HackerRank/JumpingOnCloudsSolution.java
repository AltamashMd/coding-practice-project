import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class JumpingOnCloudsSolution {

    // Complete the jumpingOnClouds function below.
    static int jumpingOnClouds(int[] c) {
		
		System.out.println("jumpingOnClouds starting ");
		int jump=0;
		for(int i=0;i<c.length;){
			
			if(i+1<c.length && c[i+1]!=1){
				
				if(i+2<c.length && c[i+2]!=1){
					jump++;
					i=i+2;
				}
				else{
					jump++;
					i=i+1;
				}
			}
			else if(i+2<c.length && c[i+2]!=1){
				jump++;
				i=i+2;
			}
			else{
				i++;
			}
		}
		
		System.out.println("jump : "+jump);

		
		return jump;

    }

    public static void main(String[] args) throws IOException {

		Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] c = new int[n];


        for (int i = 0; i < n; i++) {
            c[i] = scanner.nextInt();
        }

        int result = jumpingOnClouds(c);
		
		System.out.println("result : "+result);

    }
}
