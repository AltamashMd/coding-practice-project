import java.util.*;

public class TwoStrings {

    public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		
		int testcase = scanner.nextInt();
		
		while(testcase-- > 0){
				
			String s1 = scanner.next();
			String s2 = scanner.next();
			
			System.out.println(twoStrings(s1,s2));
		}
		

    }
	
	 public static String twoStrings(String s1, String s2) {
		 
		HashSet<Character> characterHashSet = new HashSet<Character>();
		
		if(s2.length()<s1.length()){
		
			for(int i=0;i<s1.length();i++){
				
				characterHashSet.add(s1.charAt(i));
			
			}
		
			for(int i=0;i<s2.length();i++){
			
				if(characterHashSet.contains(s2.charAt(i))){
				
					return "YES";
				}
			}
		
			return "NO";
		}
		else{
			
			for(int i=0;i<s2.length();i++){
				
				characterHashSet.add(s2.charAt(i));
			
			}
		
			for(int i=0;i<s1.length();i++){
			
				if(characterHashSet.contains(s1.charAt(i))){
				
					return "YES";
				}
			}
		
			return "NO";
		}
    }

}
