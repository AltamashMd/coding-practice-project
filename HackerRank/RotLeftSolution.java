import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class RotLeftSolution {

    
    static int[] rotLeft(int[] a, int d) {
		
		int[] leftRotatedArray = new int[a.length];
		
		for(int i=0;i<a.length;i++){
			
			leftRotatedArray[i] = a[d%a.length];
			d++;
			
		}
		
		return leftRotatedArray;
		
    }

    public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);
		
		int n = scanner.nextInt();
		
		int d = scanner.nextInt();

		int[] a = new int[n];
		
		for(int i=0;i<n;i++){
			a[i] = scanner.nextInt();
		}
		
        int[] result = rotLeft(a,d);
		
		for(int i=0;i<n;i++){
			System.out.print(result[i]+" ");
		}
		

    }
}
